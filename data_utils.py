import pandas as pd
import numpy as np


# TRAINING_FIELD = ['open', 'volume', 'close', 'high', 'low']
TRAINING_FIELD = ['close', 'volume']


def load_data(symbol):
    data = {}
    data_train = pd.read_csv('{}_train.csv'.format(symbol))
    data_test = pd.read_csv('{}_test.csv'.format(symbol))
    data['train'] = data_train
    data['test'] = data_test
    return data


data = load_data('AAPL')

class BatchGenerator():
    def __init__(self, data, batch_size=64, val_size=0.1, num_features=60, output_size=1):
        self._train = data['train']
        self._test = data['test']
        self._outputs = output_size
        self._val_size = val_size
        self._num_features = num_features
        self._batch_size = batch_size
        self._prepare()
    
    def _prepare(self):
        self.train = self._train[TRAINING_FIELD].values
        self.test = self._test[TRAINING_FIELD].values
        # Feature Scaling
#         sc = MinMaxScaler(feature_range = (0, 1))
#         training_set_scaled = sc.fit_transform(self.train)
#         self.test =  sc.fit_transform(self.test)

        X_train = []
        All_train = []
        for i in range(len(self.train), self._num_features, -1):
            if i-self._outputs-self._num_features >= 0:
                # make sure all labels have the same size 
                X_train.append((self.train[i-self._outputs-self._num_features:i-self._outputs, ], self.train[i-self._outputs:i, ][:,0]))
                All_train.append(self.train[i-self._num_features:i, ])
        X_train = np.array(X_train)
        self.all_train = np.array(All_train)
        np.random.shuffle(X_train)
        total_size = len(self.train)
        self.train = X_train[:int(total_size * (1-self._val_size))]
        self.valid = X_train[int(total_size * (1-self._val_size)):]
    
    def _batch_init(self):
        batch_holder = dict()
        return batch_holder
    
    def gen_train(self):
        i = 1
        total = len(self.train)
        batch = self._batch_init()
        while (i-1) * self._batch_size < total:
            # extract data from dict
            data = np.array(self.train[(i-1)*self._batch_size:i*self._batch_size, 0])
            labels = np.array(self.train[(i-1)*self._batch_size:i*self._batch_size, 1]) # open price
            for j in range(len(data)):
                data[j] = np.concatenate(data[j]).ravel()

            data = np.vstack(data[:]).astype(np.float32)
            labels = np.vstack(labels[:]).astype(np.float32)
            batch['data'] = data
            batch['labels'] = labels
            i += 1
            yield batch
            batch = self._batch_init()
    
    def gen_val(self):
        i = 1
        total = len(self.valid)
        batch = self._batch_init()
        while (i-1) * self._batch_size < total:
            # extract data from dict
            data = np.array(self.valid[(i-1)*self._batch_size:i*self._batch_size, 0])
            labels = np.array(self.valid[(i-1)*self._batch_size:i*self._batch_size, 1]) # open price
            for j in range(len(data)):
                data[j] = np.concatenate(data[j]).ravel()

            data = np.vstack(data[:]).astype(np.float32)
            labels = np.vstack(labels[:]).astype(np.float32)
            batch['data'] = data
            batch['labels'] = labels
            i += 1
            yield batch
            batch = self._batch_init()

    def get_test(self):
        test = self._test[TRAINING_FIELD].values
        X_test = []
        y_test = []
        for i in range(self._num_features, len(test)):
            if i-self._outputs-self._num_features >= 0:
                X_test.append(test[i-self._outputs-self._num_features:i-self._outputs, ])
                y_test.append(test[i-self._outputs:i, ][:, 0])
        X_test = np.array(X_test)
        y_test = np.array(y_test)
        X_test = X_test.reshape(X_test.shape[0], -1)
        return X_test, y_test
        
    def get_train(self):
        return self.all_train.reshape(self.all_train.shape[0], -1)
    
    def get_all(self):
        train = self._train[TRAINING_FIELD].values
        test = self._test[TRAINING_FIELD].values
        _all = np.concatenate((train, test), axis=0)
        X_all = []
        y_all = []
        for i in range(self._num_features, len(_all)):
            X_all.append(_all[i-self._num_features:i, ])
            y_all.append(_all[i][0])
        X_all = np.array(X_all)
        y_all = np.array(y_all)
        X_all = X_all.reshape(X_all.shape[0], -1)
        return X_all, y_all
